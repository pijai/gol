package it.uniba.gol;

import static org.junit.Assert.*;

import org.junit.Test;

public class GridTest {

	@Test(expected = CustomLifeException.class)
	public void invalidGrid() throws CustomLifeException{
		Cell[][] cell = new Cell[3][3];		
		new Grid (cell,0,0);
	}
	
	@Test
	public void widthSouldBeReturned() throws CustomLifeException{
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,false);
		cell[0][2] = new Cell(0,2,true);
		
		cell[1][0] = new Cell(1,0,true);
		cell[1][1] = new Cell(1,1,true);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,true);
		cell[2][2] = new Cell(2,2,false);
		Grid grid = new Grid(cell,3,3);
		assertEquals(3,grid.getWidth());
	}
	
	@Test
	public void heightSouldBeReturned() throws CustomLifeException{
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,false);
		cell[0][2] = new Cell(0,2,true);
		
		cell[1][0] = new Cell(1,0,true);
		cell[1][1] = new Cell(1,1,true);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,true);
		cell[2][2] = new Cell(2,2,false);
		Grid grid = new Grid(cell,3,3);
		assertEquals(3,grid.getHeight());
	}
	
	@Test
	public void cellSouldDie() throws CustomLifeException{
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,true);
		cell[0][2] = new Cell(0,2,true);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,false);
		cell[1][2] = new Cell(1,2,true);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,true);
		cell[2][2] = new Cell(2,2,false);
		Grid grid = new Grid(cell,3,3);
		Grid newGrid = grid.tick();
		assertEquals(4,newGrid.numberOfDeadCells());
	}
	
	@Test
	public void cellSouldAlive() throws CustomLifeException{
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,true);
		cell[0][2] = new Cell(0,2,true);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,false);
		cell[1][2] = new Cell(1,2,true);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,true);
		cell[2][2] = new Cell(2,2,false);
		Grid grid = new Grid(cell,3,3);
		Grid newGrid = grid.tick();
		assertEquals(5,newGrid.numberOfAliveCells());
	}
	
	
}
